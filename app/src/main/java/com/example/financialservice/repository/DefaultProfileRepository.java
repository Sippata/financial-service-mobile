package com.example.financialservice.repository;

import androidx.annotation.NonNull;

import com.example.financialservice.api.ApiFactory;
import com.example.financialservice.models.Authentication;
import com.example.financialservice.models.Registration;
import com.example.financialservice.models.Token;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class DefaultProfileRepository implements ProfileRepository {

    @NonNull
    @Override
    public Single<Token> auth(@NonNull Authentication authentication) {
        return ApiFactory.getApiService().getToken(authentication)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @NonNull
    @Override
    public Completable registerUser(@NonNull Registration registration) {
        return ApiFactory.getApiService().registerUser(registration)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
