package com.example.financialservice.repository;

import androidx.annotation.NonNull;

import com.example.financialservice.api.ApiFactory;
import com.example.financialservice.models.Account;
import com.example.financialservice.models.TopUp;
import com.example.financialservice.models.Transfer;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

// TODO: Кэшировать данные. Добавить получение данных из кэша.
public class DefaultAccountRepository implements AccountRepository {
    @NonNull
    @Override
    public Single<List<Account>> getList() {
        return ApiFactory.getApiService().getAccountList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @NonNull
    @Override
    public Single<Account> get(long number) {
        return ApiFactory.getApiService().getAccount(number)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @NonNull
    @Override
    public Single<Account> open() {
        return ApiFactory.getApiService().createAccount()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @NonNull
    @Override
    public Completable delete(long number) {
        return ApiFactory.getApiService().deleteAccount(number)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @NonNull
    @Override
    public Single<Account> transfer(Transfer transferInfo) {
        return ApiFactory.getApiService().transfer(transferInfo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @NonNull
    @Override
    public Single<Account> topUp(TopUp topUpInfo) {
        return ApiFactory.getApiService().topUp(topUpInfo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
