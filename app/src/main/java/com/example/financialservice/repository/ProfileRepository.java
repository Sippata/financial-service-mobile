package com.example.financialservice.repository;

import androidx.annotation.NonNull;

import com.example.financialservice.models.Authentication;
import com.example.financialservice.models.Registration;
import com.example.financialservice.models.Token;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface ProfileRepository {
    @NonNull
    Single<Token> auth(@NonNull Authentication authentication);

    @NonNull
    Completable registerUser(@NonNull Registration registration);
}
