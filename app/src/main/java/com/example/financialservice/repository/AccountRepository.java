package com.example.financialservice.repository;

import androidx.annotation.NonNull;

import com.example.financialservice.models.Account;
import com.example.financialservice.models.TopUp;
import com.example.financialservice.models.Transfer;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface AccountRepository {
    @NonNull
    Single<List<Account>> getList();

    @NonNull
    Single<Account> get(long number);

    @NonNull
    Single<Account> open();

    @NonNull
    Completable delete(long number);

    @NonNull
    Single<Account> transfer(Transfer transferInfo);

    @NonNull
    Single<Account> topUp(TopUp topUpInfo);
}
