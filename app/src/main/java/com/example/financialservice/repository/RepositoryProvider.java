package com.example.financialservice.repository;

import androidx.annotation.NonNull;

public final class RepositoryProvider {
    private static ProfileRepository profileRepository;
    private static AccountRepository accountRepository;

    private RepositoryProvider() {
    }

    @NonNull
    public static ProfileRepository provideProfileRepository() {
        if (profileRepository == null) {
            profileRepository = new DefaultProfileRepository();
        }
        return profileRepository;
    }

    @NonNull
    public static AccountRepository provideAccountRepository() {
        if (accountRepository == null) {
            accountRepository = new DefaultAccountRepository();
        }
        return accountRepository;
    }
}
