package com.example.financialservice.screen.Auth;

import com.arellomobile.mvp.MvpView;

public interface AuthView extends MvpView {
    void openAccountsScreen();

    void showLogInError();
}
