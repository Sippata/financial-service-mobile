package com.example.financialservice.screen.AccountDetail;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.math.BigDecimal;

@InjectViewState
public class AccountDetailPresenter extends MvpPresenter<AccountDetailView> {
    public void topUpAccount(long accountNumber, BigDecimal amount) {

    }

    public void transfer(long fromAccountNumber, long toAccountNumber, BigDecimal amount) {

    }
}
