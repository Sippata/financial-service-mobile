package com.example.financialservice.screen.Accounts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.financialservice.R;

import java.math.BigDecimal;

public class AccountsAdapter extends RecyclerView.Adapter<AccountsAdapter.ViewHolder> {
    private final AccountsPresenter presenter;
    private OnItemClickListener listener;

    public AccountsAdapter(AccountsPresenter presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.account_item, parent, false);

        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        presenter.onBindAccountsRowViewAtPosition(position, holder);
    }

    @Override
    public int getItemCount() {
        return presenter.getAccountsRowCount();
    }

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements AccountsRowView{
        final View view;

        TextView accountNumber;
        TextView accountBalance;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            accountNumber = view.findViewById(R.id.account_item_number);
            accountBalance = view.findViewById(R.id.account_item_balance);

            view.setOnClickListener(v -> {
                if (listener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(view, position);
                    }
                }
            });
        }

        @Override
        public void setNumber(long number) {
            accountNumber.setText(String.format("%d", number));
        }

        @Override
        public void setBalance(BigDecimal balance) {
            accountBalance.setText(String.format("%.2f RUB", balance));
        }
    }
}
