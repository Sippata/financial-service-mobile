package com.example.financialservice.screen.Accounts;

import android.view.View;

import java.math.BigDecimal;

public interface AccountsRowView {
    void setNumber(long number);
    void setBalance(BigDecimal balance);
}
