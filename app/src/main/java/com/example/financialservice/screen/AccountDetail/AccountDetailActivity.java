package com.example.financialservice.screen.AccountDetail;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.arellomobile.mvp.MvpActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.financialservice.R;
import com.example.financialservice.models.Account;
import com.example.financialservice.screen.Accounts.AccountsActivity;

public class AccountDetailActivity extends MvpActivity implements AccountDetailView {

    @InjectPresenter
    AccountDetailPresenter presenter;

    private Account account;

    private TextView numberText;
    private TextView balanceText;

    public static void start(Activity activity, @NonNull Account account) {
        Intent intent = new Intent(activity, AccountsActivity.class);
        intent.putExtra("CURRENT_ACCOUNT", (Parcelable) account);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_detail);

        account = getIntent().getParcelableExtra("CURRENT_ACCOUNT");
        numberText = findViewById(R.id.account_detail_number);
        numberText.setText(String.format("%d", account.number));
        balanceText = findViewById(R.id.account_detail_balance);
        balanceText.setText(String.format("%.2f RUB", account.balance));
    }

    public void onTopUpButtonClick(View view) {

    }

    public void onTransferButtonClick (View view) {

    }
}
