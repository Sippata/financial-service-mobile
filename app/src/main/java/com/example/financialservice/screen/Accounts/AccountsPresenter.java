package com.example.financialservice.screen.Accounts;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.financialservice.models.Account;
import com.example.financialservice.repository.RepositoryProvider;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.observers.DisposableSingleObserver;

@InjectViewState
public class AccountsPresenter extends MvpPresenter<AccountsView> {
    List<Account> accounts = new ArrayList<>();

    public void loadAccountList() {
        RepositoryProvider.provideAccountRepository().getList()
                .subscribe(new DisposableSingleObserver<List<Account>>() {
                    @Override
                    public void onSuccess(List<Account> newAccounts) {
                        accounts.clear();
                        accounts.addAll(newAccounts);
                        getViewState().refreshAccounts();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getViewState().showLoadError();
                    }
                });
    }

    public void openAccount() {
        RepositoryProvider.provideAccountRepository().open()
                .subscribe(new DisposableSingleObserver<Account>() {
                    @Override
                    public void onSuccess(Account account) {
                        accounts.add(account);
                        getViewState().refreshAccountsRange(
                                "insert",
                                accounts.size() - 1,
                                1);
                    }

                    @Override
                    public void onError(Throwable e) {
                        getViewState().showOpenAccountError();
                    }
                });
    }

    public void onBindAccountsRowViewAtPosition(int position, AccountsAdapter.ViewHolder holder) {
        Account account = accounts.get(position);
        holder.setNumber(account.number);
        holder.setBalance(account.balance);
    }

    public int getAccountsRowCount() {
        return accounts.size();
    }

    public void onAccountItemClick(int position) {
        Account account = accounts.get(position);
        getViewState().openAccountDetailScreen(account);
    }
}
