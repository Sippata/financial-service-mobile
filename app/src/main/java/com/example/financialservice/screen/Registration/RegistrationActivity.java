package com.example.financialservice.screen.Registration;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.arellomobile.mvp.MvpActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.financialservice.R;
import com.example.financialservice.screen.Auth.AuthActivity;

public class RegistrationActivity extends MvpActivity implements RegistrationView {

    @InjectPresenter
    RegistrationPresenter registrationPresenter;

    private EditText emailEdit;
    private EditText usernameEdit;
    private EditText passwordEdit;
    private EditText confirmPasswordEdit;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, RegistrationActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        emailEdit = findViewById(R.id.rEmailEdit);
        usernameEdit = findViewById(R.id.rUsernameEdit);
        passwordEdit = findViewById(R.id.rPasswordEdit);
        confirmPasswordEdit = findViewById(R.id.rPasswordConfirmEdit);
    }

    public void onRegisterButtonClick(View view) {
        registrationPresenter.registerUser(
                emailEdit.getText().toString(),
                usernameEdit.getText().toString(),
                passwordEdit.getText().toString()
        );
    }

    @Override
    public void openAuthScreen() {
        AuthActivity.start(this);
    }

    @Override
    public void showError() {
        //TODO: Show registration error.
    }
}
