package com.example.financialservice.screen.Accounts;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.arellomobile.mvp.MvpActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.financialservice.R;
import com.example.financialservice.models.Account;
import com.example.financialservice.screen.AccountDetail.AccountDetailActivity;

public class AccountsActivity extends MvpActivity implements AccountsView {

    @InjectPresenter
    AccountsPresenter presenter;

    AccountsAdapter adapter;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, AccountsActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounts);

        RecyclerView rvAccounts = findViewById(R.id.accountsList);
        adapter = new AccountsAdapter(presenter);
        rvAccounts.setAdapter(adapter);

        adapter.setOnItemClickListener((itemView, position) -> {
            presenter.onAccountItemClick(position);
        });

        presenter.loadAccountList();
    }

    @Override
    public void openAccountDetailScreen(Account account) {
        AccountDetailActivity.start(this, account);
    }

    @Override
    public void refreshAccounts() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void refreshAccountsRange(String operationType, int positionStart, int count) {
        switch (operationType) {
            case "insert":
                adapter.notifyItemRangeInserted(positionStart, count);
                break;
            case "remove":
                adapter.notifyItemRangeRemoved(positionStart, count);
                break;
            case "change":
                adapter.notifyItemRangeChanged(positionStart, count);
                break;
            default:
                throw new ArithmeticException("Unknown operation type: " + operationType);
        }
    }

    public void onOpenAccountButtonClick(View view) {
        presenter.openAccount();
    }

    @Override
    public void showLoadError() {

    }

    @Override
    public void showOpenAccountError() {

    }
}
