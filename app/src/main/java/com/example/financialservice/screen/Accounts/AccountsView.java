package com.example.financialservice.screen.Accounts;

import com.arellomobile.mvp.MvpView;
import com.example.financialservice.models.Account;

public interface AccountsView extends MvpView {
    void openAccountDetailScreen(Account account);
    void refreshAccounts();
    void refreshAccountsRange(String operationType, int positionStart, int count);

    void showLoadError();
    void showOpenAccountError();
}
