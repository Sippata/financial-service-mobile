package com.example.financialservice.screen.Auth;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.financialservice.models.Authentication;
import com.example.financialservice.models.Token;
import com.example.financialservice.repository.RepositoryProvider;
import com.example.financialservice.utils.AuthorizationUtils;

import io.reactivex.observers.DisposableSingleObserver;

@InjectViewState
public class AuthPresenter extends MvpPresenter<AuthView> {

    public void logIn(String login, String password) {
        Authentication authentication = new Authentication();
        authentication.email = login;
        authentication.password = password;
        RepositoryProvider.provideProfileRepository()
                .auth(authentication)
                .subscribe(new DisposableSingleObserver<Token>() {
                    @Override
                    public void onSuccess(Token tokenInfo) {
                        AuthorizationUtils.saveToken(tokenInfo.token);
                        getViewState().openAccountsScreen();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getViewState().showLogInError();
                    }
                });
    }
}
