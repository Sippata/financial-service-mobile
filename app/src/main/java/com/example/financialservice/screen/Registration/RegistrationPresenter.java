package com.example.financialservice.screen.Registration;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.financialservice.models.Registration;
import com.example.financialservice.repository.RepositoryProvider;

import io.reactivex.observers.DisposableCompletableObserver;

@InjectViewState
public class RegistrationPresenter extends MvpPresenter<RegistrationView> {
    public void registerUser(String email, String username, String password) {
        Registration registration = new Registration();
        registration.email = email;
        registration.password = password;
        registration.confirmPassword = password;
        registration.username = username;
        RepositoryProvider.provideProfileRepository().registerUser(registration)
                .subscribe(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        getViewState().openAuthScreen();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getViewState().showError();
                    }
                });
    }
}
