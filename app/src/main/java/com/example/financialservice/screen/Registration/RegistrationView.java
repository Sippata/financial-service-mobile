package com.example.financialservice.screen.Registration;

import com.arellomobile.mvp.MvpView;

public interface RegistrationView extends MvpView {

    void openAuthScreen();

    void showError();
}
