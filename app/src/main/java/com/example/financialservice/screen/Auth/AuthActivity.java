package com.example.financialservice.screen.Auth;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.arellomobile.mvp.MvpActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.financialservice.R;
import com.example.financialservice.screen.Accounts.AccountsActivity;
import com.example.financialservice.screen.Registration.RegistrationActivity;

public class AuthActivity extends MvpActivity implements AuthView {

    @InjectPresenter
    AuthPresenter authPresenter;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, AuthActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onLoginButtonClick(View view) {
        String login =((EditText) findViewById(R.id.usernameEdit)).getText().toString();
        String password = ((EditText) findViewById(R.id.passwordEdit)).getText().toString();
        authPresenter.logIn(login, password);
    }

    public void onSignUpButtonClick(View view) {
        RegistrationActivity.start(this);
    }

    @Override
    public void openAccountsScreen() {
        AccountsActivity.start(this);
    }

    @Override
    public void showLogInError() {
        //TODO: show log in error.
    }
}
