package com.example.financialservice.api;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.example.financialservice.utils.AuthorizationUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public final class ApiKeyInterceptor implements Interceptor {
    private ApiKeyInterceptor() {
    }

    @NonNull
    public static Interceptor create() {
        return new ApiKeyInterceptor();
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        String token = AuthorizationUtils.getToken();
        Request.Builder builder = chain.request().newBuilder()
                .addHeader("Content-Type", "application/json");
        if (!TextUtils.isEmpty(token)) {
            builder.addHeader("Authorization", String.format("%s %s", "Bearer", token));
        }
        return chain.proceed(builder.build());
    }
}
