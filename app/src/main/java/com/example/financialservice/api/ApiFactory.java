package com.example.financialservice.api;

import androidx.annotation.NonNull;

import com.example.financialservice.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiFactory {
    private static OkHttpClient okHttpClient;
    private static volatile ApiService apiService;

    private ApiFactory() {
    }

    @NonNull
    public static ApiService getApiService() {
        ApiService service = apiService;
        if (service == null) {
            synchronized (ApiFactory.class) {
                service = apiService;
                if (service == null) {
                    service = apiService = buildRetrofit().create(ApiService.class);
                }
            }
        }
        return service;
    }

    @NonNull
    private static Retrofit buildRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.HTTP_API_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(getClient())
                .build();
    }

    @NonNull
    private static OkHttpClient getClient() {
        OkHttpClient client = okHttpClient;
        if (client == null) {
            synchronized (ApiFactory.class) {
                client = okHttpClient;
                if (client == null) {
                    client = okHttpClient = buildClient();
                }
            }
        }
        return client;
    }

    @NonNull
    private static OkHttpClient buildClient() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();

        if(BuildConfig.DEBUG){
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        }

        return new OkHttpClient.Builder()
                .addInterceptor(ApiKeyInterceptor.create())
                .addInterceptor(loggingInterceptor)
                .build();
    }
}
