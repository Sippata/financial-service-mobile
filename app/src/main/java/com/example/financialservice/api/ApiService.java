package com.example.financialservice.api;

import com.example.financialservice.models.Account;
import com.example.financialservice.models.Authentication;
import com.example.financialservice.models.Registration;
import com.example.financialservice.models.Token;
import com.example.financialservice.models.Transfer;
import com.example.financialservice.models.TopUp;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiService {
    @POST("profile/token")
    Single<Token> getToken(@Body Authentication authentication);

    @POST("profile/registration")
    Completable registerUser(@Body Registration registration);

    @GET("accounts")
    Single<List<Account>> getAccountList();

    @GET("accounts/{number}")
    Single<Account> getAccount(@Path("number") long number);

    @POST("accounts/")
    Single<Account> createAccount();

    @DELETE("accounts/{number}")
    Completable deleteAccount(@Path("number") long number);

    @PATCH("accounts/transfer")
    Single<Account> transfer(@Body Transfer transfer);

    @PATCH("accounts/top_up")
    Single<Account> topUp(@Body TopUp topUp);
}
