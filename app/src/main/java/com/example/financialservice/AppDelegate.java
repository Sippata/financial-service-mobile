package com.example.financialservice;

import android.app.Application;

import com.orhanobut.hawk.Hawk;

public class AppDelegate extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Hawk.init(this)
                .build();
    }
}
