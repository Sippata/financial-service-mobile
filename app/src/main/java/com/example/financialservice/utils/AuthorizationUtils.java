package com.example.financialservice.utils;

import androidx.annotation.NonNull;

import com.orhanobut.hawk.Hawk;

public final class AuthorizationUtils {
    private static final String TOKEN_KEY = "token";

    private AuthorizationUtils() {
    }

    public static void saveToken(@NonNull String token) {
        Hawk.put(TOKEN_KEY, token);
    }

    public static String getToken() {
        return Hawk.get(TOKEN_KEY, "");
    }
}
