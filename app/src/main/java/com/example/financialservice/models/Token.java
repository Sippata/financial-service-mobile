package com.example.financialservice.models;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

public class Token {
    @SerializedName("token")
    public String token;

    @SerializedName("expireTime")
    public String expireTime;
}
