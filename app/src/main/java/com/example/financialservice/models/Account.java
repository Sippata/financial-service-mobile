package com.example.financialservice.models;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class Account {
    @SerializedName("id")
    public String id;

    @SerializedName("userId")
    public String userId;

    @SerializedName("number")
    public long number;

    @SerializedName("balance")
    public BigDecimal balance;

    @SerializedName("state")
    public String state;
}
