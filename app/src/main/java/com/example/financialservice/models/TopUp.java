package com.example.financialservice.models;

import com.google.gson.annotations.SerializedName;

public class TopUp {
    @SerializedName("accountNumber")
    public long accountNumber;

    @SerializedName("amount")
    public long amount;
}
