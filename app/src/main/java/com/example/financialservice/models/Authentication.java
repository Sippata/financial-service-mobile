package com.example.financialservice.models;

import com.google.gson.annotations.SerializedName;

public class Authentication {
    @SerializedName("email")
    public String email;

    @SerializedName("password")
    public String password;
}
