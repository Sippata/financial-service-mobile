package com.example.financialservice.models;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class Transfer {
    @SerializedName("fromAccountNumber")
    public long fromAccountNumber;

    @SerializedName("toAccountNumber")
    public long toAccountNumber;

    @SerializedName("amount")
    public BigDecimal amount;
}
