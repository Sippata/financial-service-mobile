package com.example.financialservice.models;

import com.google.gson.annotations.SerializedName;

public class Registration {
    @SerializedName("email")
    public String email;

    @SerializedName("username")
    public String username;

    @SerializedName("password")
    public String password;

    @SerializedName("confirmPassword")
    public String confirmPassword;
}
